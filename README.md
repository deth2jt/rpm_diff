## General Info

use plumb.sh
```
[userone@test orry]$ ll plumb.sh 
-rw-rw-r--. 1 userone userone 746 Apr  4 23:38 plumb.sh
```
we can create the plumbed interface

eg.
```
[vagrant@localhost ~]$ ifconfig ens33
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.128.128  netmask 255.255.255.0  broadcast 172.16.128.255
        inet6 fe80::20c:29ff:fed5:5a1c  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:d5:5a:1c  txqueuelen 1000  (Ethernet)
        RX packets 68091  bytes 28954303 (27.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 65285  bytes 12111041 (11.5 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

[vagrant@localhost ~]$ sudo sh foo.sh ens33
Restarting network (via systemctl):                        [  OK  ]
[vagrant@localhost ~]$ 
```

use rpm.py
```
[userone@test orry]$ ll rpm.py 
-rw-rw-r--. 1 userone userone 4071 Apr  4 22:22 rpm.py
```
to get list of differing rpm(s) and grab yum repo files from both servers




use below cmd to create ami:
```

PACKER_LOG=1 /usr/local/packer validate ebs.json

PACKER_LOG=1 /usr/local/packer build ebs.json
```

some simple teraform cmd ,not basing a backend or using a var file, to create an ec2 instanced based on ami:

```
terraform init

terraform plan

terraform apply
```
