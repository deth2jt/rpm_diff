#!/usr/bin/env python
#pip3 install paramiko
#https://www.middlewareinventory.com/blog/packer-aws-terraform-example/

import paramiko
import sys
import re
from functools import reduce
import os

paramiko.util.log_to_file('paramiko.log')
paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))

#python3 rpm.py 172.16.128.128  vagrant vagrant  192.168.53.148 vagrant vagrant
command1 = 'cat /etc/centos-release; rpm -qa'
command2 = "ls -l /etc/yum.repos.d/ | awk '{print $9}'"
remote_images_path = '/etc/yum.repos.d/'
local_path = 'yum.repo/'

#command = 'rpm -qa'
array = []

def rpmList(host, user, passport):
    try:
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.WarningPolicy)

        #print(sys.argv)
        #client.connect(hostname = '192.168.56.20', key_filename='/home/vmuser/.ssh/id_rsa', password = 'p@$phr4se')
        client.connect(hostname = host,  username=user, password = passport)

        #client.connect(hostname = '192.168.56.20', username="vmuser", password="1")
        
        stdin, stdout, stderr = client.exec_command(command1)
        #print(stdout.decode('utf-8')) 
        #stdout.readline()
        #print(array)
        array = str(stdout.read()[:-1]).strip("'").split('\\n')
        #print(array)
        print(len(array))

        stdin, stdout, stderr = client.exec_command(command2)
        array2 = str(stdout.read()).strip("'").split('\\n')
        array2 = array2[1:-1]

        print(array2[1:-1])

        sftp = client.open_sftp()
        for file in array2:
            file_remote = remote_images_path + file
            file_local = local_path + file + "-" + host

            print (file_remote + '>>>' + file_local)

            sftp.get(file_remote, file_local)

    finally:
        client.close()
        sftp.close()
    return array

def getMaxList(a1, a2):
    #a2 = a2[0:2]
    #eg. python-configobj-4.7.2-7.el7.noarch
    #pth-2.0.7-23.el7.x86_64
    #openssl-1.0.2k-21.el7_9.x86_64
    #polkit-0.112-26.el7.x86_64
    #gpg-pubkey-3e1ba8d5-558ab6a8 einfach nein
    #libstdc++-4.8.5-44.el7.x86_64

    returnList = []
    justinCase = []

    #for item in a1[0:2]:
    for item in a1:
        #print(item)
        #print(re.search("([0-9]+.)+", item))
        #print(re.search("-[0-9]+\.[0-9].*-[0-9]+.*.(el7)", item))
        match = re.search("^([a-z]|[A-Z])+([0-9]*[a-z]*)+(-|_)([a-z]+[0-9]*[a-z]*-)*", item)
        if match != None:
            match = match.group()
            item2 = list(filter(lambda x: x.startswith(match), a2))
            #print(item2)
            
               
            if len(item2) != 0:
                if item > item2[0]:
                    returnList += [item]
                else:
                    returnList += [item2[0]]
            else:
                justinCase += [item]
            
        else:
            #print("da nicht")
            justinCase += [item]
    justinCase += list(filter(lambda x: isNotinList(x, a1), a2))
        #print(re.search("^([a-z]|[A-Z])+([0-9]*[a-z]*)+(-|_)([a-z]+[0-9]*[a-z]*-)*", item))
    return (returnList, justinCase)
        
def isNotinList(item, lst):
    match = re.search("^([a-z]|[A-Z])+([0-9]*[a-z]*)+(-|_)([a-z]+[0-9]*[a-z]*-)*", item)
    #print(match)
    match =   " " if match == None else match.group()
    #match = match.group()
    #print(match)
    return not reduce((lambda x, y: x or y) ,list(map(lambda x: x.startswith(match), lst)))

def writeToFile(lst, fileName):
    with open(fileName, 'w') as f:
        for item in lst:
            f.write("%s\n" % item)

array1 = rpmList(sys.argv[1],  sys.argv[2], sys.argv[3])[1:]
array2 = rpmList(sys.argv[4],  sys.argv[5], sys.argv[6])[1:]
#print(array2[0])
array1 = rpmList(sys.argv[1],  sys.argv[2], sys.argv[3])[1:]
#print(len(getMaxList(array1, array2)[1]))
print(getMaxList(array1, array2)[1])
filename="rpmlist.txt"
writeToFile(array1,filename)
'''
for line in iter(array, b''):

	# Print line
	sys.stdout.flush()
	print(">>> " + line.rstrip())
    '''