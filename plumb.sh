#!/bin/bash

cat > /etc/sysconfig/network-scripts/ifcfg-$1:0 <<EOF
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
IPADDR=5.5.5.2
PREFIX=25
GATEWAY=5.5.5.1
IPV4_FAILURE_FATAL=no
DEVICE=$1:0
ONBOOT=yes
DNS1=8.8.4.4
DNS2=8.8.8.8
EOF


cat > /etc/sysconfig/network-scripts/ifcfg-$1:1 <<EOF
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
IPADDR=6.6.6.2
PREFIX=25
GATEWAY=6.6.6.1
IPV4_FAILURE_FATAL=no
DEVICE=$1:1
ONBOOT=yes
DNS1=8.8.4.4
DNS2=8.8.8.8
EOF


cat > /etc/sysconfig/network-scripts/ifcfg-$1:2 <<EOF
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
IPADDR=7.7.7.2
PREFIX=25
GATEWAY=7.7.7.1
IPV4_FAILURE_FATAL=no
DEVICE=$1:2
ONBOOT=yes
DNS1=8.8.4.4
DNS2=8.8.8.8
EOF

service network restart