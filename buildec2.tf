provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "web" {
  ami           = "ami-0ea0326a78dbfaf71"
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}